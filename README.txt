Overview:
--------
This module provides a page like the standard Site Information page (admin/settings/site-information) but with the ability for an administrator to set which fields should be shown. Permission are created for administering which fields should be shown and for accessing the "lite" site information page.

This allows creating a site where editors can: 
 - edit the site info but not break the site by changing settings for modules, performance/cache, clean-urls, etcetera,
 - edit things like the site name, email and slogan but not the footer (eg if it contains links to the web designer or d.o that shouldn't change) or default front page (breaking the front page if the editor doesn't know what they're doing).


Requires:
--------
 - Drupal 5.0


Credits:
-------
 - Author: Oliver Coleman, oliver@e-geek.com.au
 - http://e-geek.com.au
 - http://enviro-geek.net
